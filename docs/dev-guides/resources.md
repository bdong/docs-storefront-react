---
id: resources
title: Related Resources that may be helpful
sidebar_label: Related Resources
---

## Elastic Path Commerce Resources

- [Elastic Path Documentation Site](https://documentation.elasticpath.com/)
- [Cortex API Front-End Documentation](https://documentation.elasticpath.com/commerce/docs/cortex/index.html)
- [Cortex API Guide: Back-end development](https://documentation.elasticpath.com/commerce/docs/cortex/backend-dev/api-definition.html)
- [Elastic Path Commerce Documentation](https://documentation.elasticpath.com/commerce/docs/core/index.html)

## References

The following resources provide more information on the third-party technologies that Elastic Path uses for developing React Reference Storefront:

- [React](https://reactjs.org/)
- [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
- [Webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)
- [Workbox](https://developers.google.com/web/tools/workbox/)
- [Storybook](https://storybook.js.org/)
