---
id: extending
title: Extending React Reference Storefront
sidebar_label: Extending the Storefront
---

To extend the Storefront, add more pages. If you want to create custom components for a page, see [Creating new components](consuming.md#creating-new-components).

## Adding pages in React

1. In the `app/src/containers` directory, create a new `.tsx` file for the page.

	For example, if the page name is `MyPage`, the file name is `app/src/containers/MyPage.tsx`.

2. Populate the page with the required structure by copying the contents of an existing page to the new page.
3. Name the class of page with page name.
4. In the `app/src/routes.ts` directory:
	- Import the new page.
	- Define the desired routing path for the page.
5. In the `.tsx` file of the page:
	- To view the changes in the storefront, update the export settings with page name.
	- Add all required components and content.
6. In the `app/src/containers/<PageName>.less` file, add the required custom CSS.
