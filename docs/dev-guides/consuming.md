---
id: consuming
title: UI Components
sidebar_label: UI Components
---

The Reference Storefront React UI components are open source. You can reuse the components to implement a storefront for the Elastic Path platform or for any other front-end implementation. For more information, see the [Elastic Path licensing agreement](https://github.com/elasticpath/react-pwa-reference-storefront/blob/master/LICENSE).

## Component library

You can find the source code for the components in GitLab and the components package in NPM (Node Package Manager).

-	In GitLab, the components source code is in the `react-pwa-reference-storefront/components` directory.
-	In NPM, the components package is available at `@elasticpath/store-components`.

The following diagram shows the file structure for the component library and identifies the types of files:

```bash
components
└───src
│   └───CartMain
│   │   │   cart.main.less                // Style definition
│   │   │   cart.main.stories.tsx         // Storybook story (optional)
│   │   │   cart.main.tsx                 // Component definition
│   │
│   └───CartLineItem
│   │   │   cart.lineitem.less
│   │   │   cartlineitem.stories.tsx
│   │   │   cart.lineitem.tsx
│   │
│   │   ...
│
│   ...
```

## Component styles

After you verify that the default implementation of a component works in your application, you can update the style of the components to match the look-and-feel of your application. For style definitions, the Storefront uses Less, which is an open source language extension for CSS.

Each Storefront component includes a `.less` file that defines the style for the component. Component `.less` files inherit styles from the application theme, which is located in the `app/src/theme/common.less` file. You can change the look-and-feel of a storefront by modifying the corresponding `.less` files.

## Consuming components

Add components as elements in your application. You can override the default behavior by setting properties for data and configuration.

1.	Import the component from the `@elasticpath/store-components` package.

    ```javascript
    import { CartMain } from '@elasticpath/store-components';
    ```

2.	Create an element for the component in your application.

    For example, the following sample code shows an element that defines the `CartMain` component. Each assignment that includes the prefix `this` overrides a default property of the component.

    ```javascript
    <CartMain
      empty={!cartData['total-quantity'] || cartData._lineitems === undefined}
      cartData={cartData}
      handleQuantityChange={() => { this.handleQuantityChange(); }}
      onItemConfiguratorAddToCart={this.handleItemConfiguratorAddToCart}
      onItemMoveToCart={this.handleItemMoveToCart}
      onItemRemove={this.handleItemRemove}
      itemDetailLink={itemDetailLink}
    />
    ```

3.	In your application, verify that the component displays.

## Styling components

After you verify that the implemented component works in your application, you can update the style of the component to match the look-and-feel of your application.

**Note:** Style changes are displayed in your application, not in Storybook.

1.	Go to the component subdirectory.
2.	Edit the `.less` file to change the style definitions.

    For example, to change the `CartMain` component style, edit the following file:   `react-pwa-reference-storefront/components/src/CartMain/cart.main.less`

3.	In your application, verify that the component reflects the style changes.

## Creating new components

You can design and create new components as needed.

**Tip:** If you use Storybook, you can add stories for your custom components to Storybook. For more information, see [Creating Storybook Stories](storybook.md#creating-storybook-stories).

1.	In the `components/src` directory, create a new directory with the name of the component.

    **Note:** Directory names can contain letters, numbers, dashes, and underscores. Don’t use spaces or special characters.

2.	Copy the contents of an existing component to the new component to populate the component with the required structure, and rename the files with the same name as the directory.
3.	Name the class of the component with the component name.
4.	In the `components/src/mycomponent/mycomponent.main.less` file, define custom styles as necessary.
