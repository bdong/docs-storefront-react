---
id: implement-b2b
title: Configuration and Implementation for B2B
sidebar_label: Implement B2B
---

## Account Management Service for React PWA Reference Storefront

The React PWA (Progressive Web App) Reference Storefront uses the Account Management service to authenticate any B2B Commerce shopping flows.

For more information about the `b2b` configurations in the `app/src/ep.config.json` file, see [Configuration Parameter Descriptions](https://documentation.elasticpath.com/storefront-react/docs/setup.html#configuration-parameter-descriptions).

With the B2B configuration in the React PWA Reference Storefront, the storefront displays the new user interface components and provides B2B shopping features, such as:

- [Quick Order](https://documentation.elasticpath.com/commerce/docs/cortex/frontend-dev/bulk-order.html#quick-order-tab)
- [Bulk Order](https://documentation.elasticpath.com/commerce/docs/cortex/frontend-dev/bulk-order.html)
- Barcode Scanner
- Login through Account Management
- [B2B Shopping flow with Account selection](https://documentation.elasticpath.com/account-management/docs/workflows.html)
- SKU display in Product Display Pages
- [Multiple Carts](https://documentation.elasticpath.com/commerce/docs/cortex/frontend-dev/cart-items.html)
- [Buyer Administrator Changing Settings](https://documentation.elasticpath.com/account-management/docs/index.html)

## Implementing Keycloak Theme

### Prerequisites

Ensure that the following parameters are configured as required:

- `cortexApi.scope`: The store or organization name used by the buyer organization divisions for your store
- `cortexApi.pathForProxy`: The location of cortex instance used for any B2B Commerce shopping flows

To enable the barcode scanner feature to locate products, ensure products/SKUs have a corresponding UPC (Universal Product Code) added to their attributes. For more information on setting attributes, see [Elastic Path Commerce Manager Available Attributes and Assigned Attributes](https://documentation.elasticpath.com/commerce-manager/docs/user-guides/catalog-management.html#product-types-tab).

### About this task

By default, Keycloak provides a set of themes, such as login or accounts themes, that may be used for various actions that Keycloak supports. Use one of these existing themes for the supported flows or add a customized theme to the Keycloak Docker Image, if required. The React PWA Reference Storefront provides a login theme to use for the custom login page. For more information, see the [Elastic Path Account Management Deployment Guide](https://documentation.elasticpath.com/account-management).

For the current KeyClock Docker Image, add the custom themes to the `/devops/docker/keycloak/themes` directory.

### Procedure

To apply a custom Keycloak theme to an Account Management instance:

1. Copy and extract the `https://github.com/elasticpath/react-pwa-reference-storefront/tree/master/am/keycloak/themes.zip` file into the `/devops/docker/keycloak/themes` directory.
    - **Note:** Ensure that the new directory is in the following order: `/devops/docker/keycloak/themes/vestri/...`
2. Build and run the Account Management KeyClock Docker Image.
    - For more information, see the [Deploying and Configuring Keycloak](https://documentation.elasticpath.com/account-management) section
3. To use the newly added `vestri` custom theme, configure your client in the KeyClock administrative console

## Related Documentation

- [Elastic Path Account Management](https://documentation.elasticpath.com/account-management/)
