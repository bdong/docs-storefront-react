---
id: best-practices
title: Recommendations or Best Practices
sidebar_label: Best Practices
---

## Running a Linter

The storefront project is set up with the linting utility, [ESLint](https://eslint.org/). For the storefront project, Elastic Path uses and extends the ESLint configuration provided by Airbnb. For more information about the style guide, see the [Airbnb GitHub](https://github.com/airbnb/javascript) page.

By default, the ESLint loader is added to the `webpack.config.dev.js` file. When you start the application in the development mode, the ESLint loader automatically runs.

1. To run the linter from the command line, navigate to the project root directory
2. Run the following command:
    `./node_modules/.bin/eslint --ext .js --ext .jsx [file|dir|glob]`
3. Run the following command for the entire project:
 `./node_modules/.bin/eslint --ext .js --ext .jsx .`
With the ESLint extension for Visual Studio Code, you can view feedback when you write the code in the `Problems` view

**Note:** When you check in the code, ensure that all linting errors are resolved.

## Best Practices for Extending React Reference Storefront

Elastic Path recommends following the best practices listed in this section. However, Elastic Path does not recommend any specific rule for creating a page or component.

* Base the storefront page as the actual page your shoppers visit in the storefront
* Design a page to have a component corresponding to each functionality in the page. The functionality for a component, including references to the child components, are are configured within the component. You can customize components with properties and parameters to perform various actions, if required.
    * For example, a shopper can navigate to a category page to view product lists. This category page can have a component to display the products within the category, and that component can have another component for each of the products in the list. The component that displays the products in a category view can be used to display products on the search results page because of the similar functionality between the workflows

## Global Product Attribute

* Use the formatted value of `store-name:category`, such as `vestri:Accessories`, to submit the product category information. Google Analytics handlers use the global product attribute `Tag`. You can set the global attribute for each product in the catalog to avoid submitting empty values for the product’s category to Google Analytics.

* Use [Elastic Path Dynamic Bundle Accelerator](https://code.elasticpath.com/accelerators/dynamic-bundles) for implementing Dynamic Bundles for products. For more information about the accelerator license, see the [License and Copyright page](https://www.elasticpath.com/sites/default/files/elastic-license-and-copyright.pdf)
