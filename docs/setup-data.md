---
id: setup-data
title: Setup sample data
sidebar_label: Setup Sample Data
---

## Configuring Sample Data

Elastic Path provides a set of sample data with the storefront project.

**Note:** A valid Elastic Path development environment is required to access the sample data.

1. From the `react-pwa-reference-storefront/data` directory, extract the sample catalog data contents into the `ep-commerce/extensions/database/ext-data/src/main/resources/data` directory
2. In the `ep-commerce/extensions/database/ext-data/src/main/resources/data/` directory, update the `liquibase-changelog.xml` file with the following sample data:

```xml
<include file="ep-blueprint-data/liquibase-changelog.xml" relativeToChangelogFile="true" />
```

**Note:** This data must be the only sample data included within the sample data block
3. In the  `ep-commerce/extensions/database/ext-data/src/main/resources/environments/local/data-population.properties` file, set the `liquibase.contexts` property to `default,ep-blueprint`:

```xml
liquibase.contexts=default,ep-blueprint
```

**Note:** This must be the only liquibase context configured
4. In the command line, navigate to the `ep-commerce/extensions/database` module
5. To run the Data Population tool, run the following command:
    * `mvn clean install -Preset-db`

For more information about populating database, see the [Populating the Database](https://developers.elasticpath.com/commerce/7.3/Core-Commerce-Development/Setting-up-your-Developer-Environment/Populate-the-Database#ConfigureDemoDataSets) section.
