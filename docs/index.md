---
id: index
title: Overview of React PWA Reference Storefront
sidebar_label: Overview
---

## Introduction

The React PWA Reference Storefront is a flexible e-commerce website built on Elastic Path’s [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) e-commerce API,  [Cortex API](https://developers.elasticpath.com/commerce/7.3/Cortex-API-Front-End-Development/Getting-Started/Introduction). Through Cortex API, the storefront get to use the e-commerce capabilities provided by Elastic Path Commerce and get data in a RESTful manner.

React Reference Storefront is composed of containers (pages) and React components. Each page in the storefront is composed of various components that fulfills the purpose of the page. For example: home, cart, categories, checkout, and registration are separate pages. Each page consists of various components, such as navigation, footer, product list, or products.

The Storefront is designed as an open source mobile Progressive Web Application (PWA) that has the capabilities for local browser storage page caching and persistent session management.
This PWA is built using the following technologies:

* [React.js](https://reactjs.org/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [Babel](https://babeljs.io/)
* [Webpack](https://webpack.js.org/)
* [node.js](https://nodejs.org/en/)

The Storefront uses [Storybook](https://storybook.js.org/) as a tool to build UI components in isolation. It organizes them for consumption in other implementations that may use the components.

With theming, you can change the presentation of the React Reference Storefront. React Reference Storefront uses the dynamic stylesheet language, [less](http://lesscss.org/), to change the styling of the store. You can change the look and feel of the storefront by modifying the corresponding `{less}` files.

Each component and page contains `{less}` files, which can be compiled into a `style.css` file. The `{less}` files for each component/page are organized in the same order as the storefront is presented.
For example, `CartPage.less` contains CSS for the cart’s container page, and `cart.main.less` contains CSS for the main component in the cart page.

## Features

In React Reference Storefront, e-commerce functionality such as cart, authentication, profile, search are part of the respective components. Each component makes the independent Cortex calls to retrieve the data it needs from the backend. The front-end developers need not change JavaScript to update the CSS files to style/brand their store. Each customization layer is separated into it’s own layer from the core code, so developers need not change the Storefront’s utility engine unless desired.

You can access the React Reference Storefront on various devices, such as tablets, mobile phones, or computers.

![Feature Guide](assets/featureSupport.png)

## Platform Architecture

The React PWA Reference Storefront is composed of layers that work together to provide various capabilities to operate the storefront. The following diagram shows the storefront architecture:

![Reference Store Architecture](assets/Ref_Store_Architecture.png)

* **Presentation Layer**: Provides the user interface management capabilities using a modern UI framework and routes browser requests to the subsequent pages. The files in this layer are:
    * `index.html` - The front-end html document that is populated by the React pages and components
    * `App.tsx` - The javascript file that contains the import instructions for the React components and the subsequent layers. When this script is run, all subsequent components are loaded on the page
    * `index.tsx` - The javascript file that contains the import instructions for the required frameworks, such as bootstrap, React, and renders the `App.tsx` file. This file also registers the PWA service worker
    * CSS - The file that consists of the custom stylesheets and static page assets for the storefront front-end presentation
    * `routes.ts` - The javascript file that contains routes and mappings to the React pages
    * Service worker - A service worker created using Workbox to manage caching and PWA capabilities
* **Catalog & Site Images**: Externally hosted catalog and site content images. Can be hosted on a CMS (Content Management System) or CDN (Content Delivery Network) of choosing
* **Page & Component Layer**: The top-level pages that loads the required components for each page. Provides the components to be loaded within each page. The components are created depending on the functionality of the item that the pages interact with or loads. For example, the component layer provides the login component for the home page
* **Web Server Layer**: Enables the interaction between the application and the service layer
* **API Layer**: The application layer of React PWA Reference Storefront is built on [Cortex API](https://developers.elasticpath.com/commerce/7.3/Cortex-API-Front-End-Development/Getting-Started/Introduction). Through the Cortex API, the storefront get to use the e-commerce capabilities provided by Elastic Path Commerce. PWA interacts with the required services through this layer

### Cortex Integration

The storefront invokes Cortex APIs to ensure that the best practices of consuming hypermedia APIs are followed.

![Cortex](assets/cortex-page-diagram.png)

By modifying the following files, you can customize the API calls that make the REST HTTP request:

* The `Cortex.ts` file contains the methods and classes that fetch data for the API calls made to Cortex. You can customize this file with additional headers or generic request modifications in the `cortexFetch()` function for custom implementations. This file also includes `x-headers` and the ability to add any additional headers
* The `AuthService.ts` contains the authentication functions used to authenticate users with Cortex using OAuth2. The OAuth tokens are saved in the local storage of the web application. For authenticating subsequent Cortex requests, the tokens are retrieved and added into the header
* The `CortexLookup.ts` contains the functions used to perform lookups on the forms provided by Cortex, and saves them in the local storage of the web application

You can create zoom queries by populating an array with all required queries and appending it to the request to Cortex. When you create a zoom query, request only the additional data needed by the component using the query. Elastic Path recommends to avoid requesting unnecessary data, and circular zooms for better performance.

### Workflow

1. The top-level root for the REST call is retrieved from Cortex. This URL consists of related links that provides URLs to the children of the root
2. After retrieving the root level, subsequent actions are performed against Cortex on the children of the root, depending on the action intended to be performed by the React Component initiating the REST request

> **Note**: For the subsequent calls, the storefront must call only the URL that Cortex returns when you call the root. You must not create custom URL in the storefront component settings to ensure that any change in the API names do not change the URL. Use the `rels` in Cortex response to make subsequent calls. However, for the OAuth implementation, you might want to create a URL in the storefront.
