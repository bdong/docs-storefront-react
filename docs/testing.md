---
id: testing
title: Running tests and adding locales
sidebar_label: Testing and Localization
---

## Best Practices for Testing

By default, Elastic Path provides automated tests for various end to end flows of the React PWA (Progressive Web App) Reference Storefront. You can run the tests directly on the development environment that is being used to implement the Reference Storefront.

If you would prefer to run tests against a different environment, you must first ensure to set the environment variable `TEST_HOST` to the location of your storefront instance:

* Example: `export TEST_HOST=http://<instance_host>:<port>`

Proceed with the following instructions.

The end to end tests make use of the sample data provided with the React PWA Reference Storefront. For more information, see the [Configuring Sample Data](https://documentation.elasticpath.com/storefront-react/docs/setup.html#configuring-sample-data) section.

When you create tests:

* Create additional automated tests when you add new functionality
* Run automated tests to ensure that no regression is introduced after customizing the project

## Running End to End Tests

The tests are provided in the `tests` directory.

1. To run all unit tests, run the following command: `yarn test`
2. To run all end to end tests:
    1. Run the `yarn test` command and press **p**
    2. Enter `e2e` and press **Enter**
3. To run a single test:
    1. Run the `yarn test` command and press **p**
    2. Enter the filename

**Note:** You might need to install watchman to get past the "too many files open" error. To install watchman, use [Homebrew](https://brew.sh/):

* `brew update`
* `brew install watchman`

## Adding New Locales

The reference storefront supports multiple languages and currencies. Add all front-end textual data in the `localization/en-CA.json` file as a resource string. By default,  `en-CA` and `fr-FR` locales are provided in the project.

1. For development purpose, run: `node tools/translate.js`. This runs a pseudo translation from `en-CA` locale to `fr-FR`
2. To add a new locale, add an entry to the `supportedLocales` array in the `ep.config.json` file and add an appropriate `.json` file to the `localization` folder
3. Configure the language and currency for all products in Commerce Manager

For more information, see [Puppeteer](https://github.com/GoogleChrome/puppeteer/blob/v1.12.2/docs/api.md).
