---
id: setup
title: Setup your development environment
sidebar_label: Setup
---

## Start building your storefront

You need an Elastic Path platform which is used as the backend of this storefront.

The storefront defaults to a B2C instance. For more information about configuring the storefront as a B2B instance, see [Implement B2B](https://documentation.elasticpath.com/storefront-react/docs/dev-guides/implement-b2b.html).

```bash
# Clone the Git repository
git clone https://github.com/elasticpath/react-pwa-reference-storefront.git

# Go into the cloned directory
cd react-pwa-reference-storefront

# Install all the dependencies for all sub-project and create necessary symlinks in-between them
yarn

# Configure the ./app/src/ep.config.json file.
# For more information, see Configuration Parameter Descriptions.

# Start the app in development mode

# Run the main application:
cd app
yarn start

# Optionally run the Storybook application:
cd storybook
yarn storybook
```

## Configuration Parameter Descriptions

Parameters that require configuration are in the `./app/src/ep.config.json` file:

|  Parameter| Importance|Type|Description|
|--|--|--|--|
|`cortexApi.path`| Required| String| The URL, which is composed of the hostname and port, to access Cortex. By default, a web proxy is configured in the [Webpack](https://webpack.js.org/) configuration of the project. For local development, set this value to `/cortex` to redirect Cortex calls to the local proxy.|
|`cortexApi.scope`| Required| String| Name of the store from which Cortex retrieves data.|
|`cortexApi.pathForProxy`|Required|String| The path to which the [Webpack](https://webpack.js.org/) proxy routes the Cortex calls from the storefront. This value is a URL that consists of hostname and port of a running instance of Cortex. Leave this field blank to disable proxy.|
|`cortexApi.reqTimeout`| Optional| String| Maximum time to wait for a Cortex API request to time out. Default is 30000 milliseconds.|
|`skuImagesUrl`| Required| String| The URL that consists of the path to catalog images hosted on an external CMS (Content Management System). Set this parameter to the complete URL of the images by replacing the `sku/file-name` with the `%sku%` string. This value is populated when the page is loaded with values retrieved by Cortex.|
|`siteImagesUrl`| Optional| String| The path to the site content and marketing images hosted on an external CMS. Set this parameter to the complete URL of the images by replacing the filename and file extension with `%fileName%`. This parameter is populated with the values set in the components when the page is loaded, and uses the assets locally available in the `./app/src/images/site-images` directory.|
|`arKit.enable`| Optional| Boolean| Enable elements for ARKits Quick Look capability to load on a product display page. When `arKit.enable` is enabled, any product images that have hosted ARKit USDZ files are wrapped with an anchor tag referencing the file hosted on an external CMS.|
|`arKit.skuArImagesUrl`| Optional| String| The path to the USDZ (Universal Scene Description Zip) files hosted on an external CMS used for ARKit Quick Look. Set this parameter to the complete URL of the files by replacing the `sku/file-name` with `%sku%`. This value is populated when the page is loaded with values retrieved by Cortex.|
|`b2b.enable`| Optional| Boolean| The general configuration to enable the B2B e-commerce shopping flow components in the storefront.|
|`b2b.authServiceAPI.path`| Optional| String| The URL, which is composed of the hostname and port, to access the Admin APIs. By default, a web proxy is configured in the [Webpack](https://webpack.js.org/) configuration of the project. For local development, set this value to `/admin` to redirect Cortex calls to the local proxy.|
|`b2b.authServiceAPI.pathForProxy`| Optional| String| The path to which the [Webpack](https://webpack.js.org/) proxy uses to route Cortex calls from the storefront to the publicly hosted Account Management service. This value is a URL consisting of a hostname and port of a running Cortex for Account Management service instance. Leave this field blank to disable proxy.|
|`b2b.authServiceAPI.reqTimeout`| Optional| String| Maximum time to wait for a Account Management API request to time out. Default is 30000 milliseconds.|
|`b2b.keycloak.callbackUrl`| Optional| String| The URL that is passed to keycloak, and redirected to upon a successful login.|
|`b2b.keycloak.loginRedirectUrl`| Optional| String| The keycloak log on URL.|
|`b2b.keycloak.logoutRedirectUrl`| Optional| String| The keycloak log off URL.|
|`b2b.keycloak.client_id`| Optional| String| The keycloak client configuration ID. Set this value to the identifier of the client configured in keycloak for the storefront. A default configuration is preset, however older environments may use `eam`. For more information, see the [Deploying and Configuring Keycloak](https://documentation.elasticpath.com/account-management/docs/deployment/index.html) section.|
|`gaTrackingId`| Optional| String| The Google Analytics tracking ID to integrate with Google Analytics Suite to track enhanced e-commerce activity on the site.|
|`indi.enable`| Optional| Boolean| Enable the integration component for Indi. For more information, see `https://indi.com/`.|
|`indi.carousel`| Optional| Values| Configurations for the Indi carousel component.|
|`indi.productReview`| Optional| Values| Configurations for the Indi product review component.|
|`indi.brandAmbassador`| Optional| Values| Configurations for the Indi brand ambassador component.|
|`facebook.enable`| Optional| Boolean| Enable the Elastic Path Facebook Chatbot component. For more information, see `https://github.com/elasticpath/facebook-chat`.|
|`facebook.pageId`| Optional| String| The Facebook Page Identifier used to connect the chatbot component to Facebook.|
|`facebook.applicationId`| Optional| String| The Facebook Application Identifier used to connect the chatbot component to Facebook.|
|`GDPR.enable`| Optional| Boolean| Enable the GDPR (General Data Protection Regulation) modal in the storefront.|
|`PowerReviews.enable`| Optional| Boolean| Enable the integration component for PowerReviews. For more information, see `https://www.powerreviews.com/`.|
|`PowerReviews.api_key`| Optional| String| The PowerReviews API Key used to connect the PowerReviews component.|
|`PowerReviews.merchant_group_id`| Optional| String| The PowerReviews Merchant Group Identifier used to connect the PowerReviews component.|
|`PowerReviews.merchant_id`| Optional| String| The PowerReviews Merchant Identifier used to connect the PowerReviews component.|
|`formatQueryParameter.standardlinks`| Optional| String| The configuration passed to Cortex to request standard and slimmer links without uri and rev. Use this parameter to get smaller zoom responses. The storefront may behave different than usual with this enabled. |
|`formatQueryParameter.noself`| Optional| String| The configuration passed to Cortex to omit self in zoomed resources. The storefront may behave different than usual with this enabled. |
|`formatQueryParameter.nodatalinks`| Optional| String| The configuration passed to Cortex to request standard and slimmer links without data. Use this parameter to get smaller zoom responses. The storefront may behave different than usual with this enabled. |
|`creditCardTokenization.enable`| Optional| Boolean| The enablement configuration for the Cybersource tokenization requests using the Lambda for the Cybersource accelerator. |
|`creditCardTokenization.lambdaURI`| Optional| String| The URL (can be absolute or relative) to the Lambda used for the Cybersource tokenization requests for the Cybersource accelerator |
|`creditCardTokenization.overrideCustomReceiptURI`| Optional| String| The URL (can be absolute or relative) to the page used for the redirect from Cybersource to the storefront upon successful payment processing for the Cybersource accelerator |
|`creditCardTokenization.overrideCustomCancelURI`| Optional| String| The URL (can be absolute or relative) to the page used for the redirect from Cybersource to the storefront upon an un-successful payment processing for the Cybersource accelerator |
|`chatbot.enable`| Optional| Boolean| Enable the Elastic Path Reference Conversational Interface component. For more information, see `https://github.com/elasticpath/lex-chatbot`. |
|`chatbot.name`| Optional| String| The Lex Chatbot name used to connect the Reference Chatbot to Amazon Lex. |
