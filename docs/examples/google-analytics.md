---
id: google-analytics
title: Google Analytics
sidebar_label: Google Analytics
---

## Google Analytics Integration

The React PWA (Progressive Web App) Reference Storefront is pre-configured for integration with the Google Analytics enhanced e-commerce plugin.

![Google](assets/Google-page-diagram.png)

Configuration properties for Google Analytics are:

* `gaTrackingId`: The Google Analytics tracking ID to integrate with Google Analytics Suite to track enhanced e-commerce activity on the site

### Route/Page Views

In the `App.tsx` file, integration handlers for Google Analytics are pre-configured to log routes as page views in a sites real-time traffic data. Configure Google Analytics for additional measurements for conversion rates based on the page views in the web traffic.

The `Analytics.js` file contains the functions used for logging page views with a React library for invoking Google Analytics functions (ReactGA), and enhanced e-commerce capabilities.

## Checkout Flow

Along with the configurations for submitting transaction information to Cortex, the storefront’s check-out flow submits purchase details through Google Analytics’ enhanced e-commerce capabilities where the transactions are logged.  When a customer completes a transaction, the storefront provides the transaction details for Google Analytics to analyze the logged transaction and purchase amounts and cart information in the *Conversions > Ecommerce* section.
For example, when a customer views a product, adds a product to the cart, or removes a product from the cart:

* The product and cart pages submit this information to Cortex
* The product and cart pages submit this information for further analysis by Google Analytics’ in a separate request
