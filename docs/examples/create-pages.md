---
id: create-pages
title: Creating Reference Storefront Pages and Components
sidebar_label: Create Pages & Components
---

## Requirements

* A development environment
* An Elastic Path training virtual machine

## Prerequisites

Ensure that you are familiar with the following third-party technologies:

* Git
* Yarn
* Node.js
* Visual Studio Code
* Javascript
* React

## Example

In this section, [Extending React Reference Storefront](../guides/extending.html) is explained with an example.

> **Warning**: Ensure that the name of the container directory and components directory are the same as the corresponding directories. For example, if the name of the page is `MyPage`, name the container `MyPage`.

1. Navigate to `app/src/containers` directory
2. Create a new directory with appropriate name
3. In the `app/src/containers/<PageName>` directory, create a new `.jsx` file. For example, `app/src/containers/MyPage/MyPage.jsx`
4. Populate the page with the required structure by copying the contents of an existing page to the new page
5. In the `app/src/routes.ts` directory:
    * Import the new page
    * Define the routing path for the page
6. In the `.jsx` file:
    * To view the changes in the storefront, update the export settings with the page name
    * Add all required components and content
7. In the `app/src/containers/<PageName>/<PageName>.less` directory, add the required custom CSS
8. In the `components/src` directory, create a new component directory with the appropriate name
9. In the `components/src/<componentName>` directory, create a new `.jsx` file. For example, `components/src/mycomponent.main.jsx`
10. Populate the file with the required structure by copying the contents of an existing component to the new component
11. Name the class of the component with the component name. For example, for `ProductDisplayItemMain` component, class name is `ProductDisplayItemMain`
12. In the `.jsx` file:
    * To view changes in the storefront, update the export settings with the page name
    * Add all required components and content
    * Import the component to other components or pages as required
13. In the `components/src/mycomponent.main.less` file, create any custom CSS
14. In the `components/src/mycomponent.main.jsx` file, import any custom CSS file
