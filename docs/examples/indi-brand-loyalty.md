---
id: indi
title: Third-Party User Generated Content Integration
sidebar_label: Indi Integration
---

## Brand Loyalty Integration for Indi

The React PWA (Progressive Web App) Reference Storefront is pre-configured with a custom component for Indi integration. This component interacts with various widgets that Indi displays. When Indi adds new widgets, you can update the component with the new widget. For more information on Indi, see the company [website](https://indi.com/).

The Indi component is a stand-alone component to fetch only the `indi-embed` libraries and display the elements corresponding to the libraries. These elements are passed to the component as a structured list.

![indi](assets/indi-page-diagram.png)

This component requires the following parameters:

* `render`: The list of the elements to display from Indi. For example, `render={['carousel', 'brand', 'product']}`.
Note:  Provide the correct configurations to render the element correctly
* `configuration`: The structured object that consists of the configuration required for the Indi components. These configurations are defined in the `indi` parameter in the `react-pwa-reference-storefront⁩/app/src/ep.config.json` file
* `keywords`: A string of the keywords for the product in which the Indi component is displayed

Static strings for the Indi component are localized in the Reference Storefront. These strings can be overridden when the component is used. With this setting, the component displays the storefront in the locale that a customer chooses. The localized strings for the Indi component are available in the corresponding language files in the `app/src/localization` directory. This setting is only for the strings with the `indi-` prefix.

The following are the configuration settings for the indi parameters in the `ep.config.json` file:

| **Functionality**| **Parameter**| **Description**|
|--|--|--|
| **Integration**|`indi.enable`| The parameter that enables the component for the integration with Indi. For more information about the required configurations, see the company [website](https://indi.com/).|
| **Theming**|`indi.carousel`| The configurations for the Indi carousel component.|
| |`indi.carousel.apikey`| The _apikey_ that connects a carousel to Indi.|
| |`indi.carousel.id`| The id used to connect the carousel to Indi.|
| |`indi.carousel.size`| The theme parameter used to display the Indi content for the carousel component.|
| |`indi.carousel.theme`| The theme parameter used to display the Indi content for the carousel component.|
| |`indi.carousel.round_corners`| The theme parameter used to display the Indi content for the carousel component.|
| |`indi.carousel.show_title`| The theme parameter used to display the Indi content for the carousel component.|
| |`indi.carousel.show_views`| The theme parameter used to display the Indi content for the carousel component.|
| |`indi.carousel.show_likes`| The theme parameter used to display the Indi content for the carousel component.|
| |`indi.carousel.show_buzz`| The theme parameter used to display the Indi content for the carousel component.|
| |`indi.carousel.animate`| The theme parameter used to display the Indi content for the carousel component.|
| **Product Review**|`indi.productReview`| Configurations for the Indi product review component.|
| |`indi.productReview.apikey`| The _apikey_ that connects a carousel to Indi.|
| |`indi.productReview.id`| The id used to connect the carousel to Indi.|
| |`indi.productReview.reviews_title`| The reviews component title.|
| |`indi.productReview.size`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.theme`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.round_corners`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.show_title`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.show_views`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.show_likes`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.show_buzz`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.animate`| The theme parameter used to display the Indi content for the review component.|
| |`indi.productReview.submit_link_label`| The label used for the button to submit reviews to Indi.|
| |`indi.productReview.allowanon`| The configuration to enable anonymous reviews to be submitted to Indi.|
| |`indi.productReview.return_url`| The URL to redirect to from Indi.|
| |`indi.productReview.show_submit_thumbnail`| The thumbnail provided by Indi for the submit button.|
| |`indi.productReview.submit_prompt_title`| The title used for the component upon submitting a review.|
| |`indi.productReview.submit_prompt_desc`| The description used for the component upon submitting a review.|
| **Brand Ambassador**|`indi.brandAmbassador`| The configurations for the Indi brand ambassador component.|
| |`indi.brandAmbassador.title`| The title used for the brand ambassador component.|
| |`indi.brandAmbassador.description`| The description used for the brand ambassador component.|
| |`indi.brandAmbassador.submit_button_url`| The URL of the submit button provided by Indi.|
| |`indi.brandAmbassador.thumbnail_url`| The location of the thumbnail for the submit button.|
| |`indi.brandAmbassador.return_url`| The URL to redirect to from Indi.|

## Related Documentation

* [Indi](https://indi.com/)
