---
id: site-branding
title: Branding your site
sidebar_label: Site Branding
---

## Requirements

* A development environment
* An Elastic Path training virtual machine

## Prerequisites

Ensure that you are familiar with the following third-party technologies:

* Git
* Yarn
* Node.js
* Visual Studio Code

## Example

**Warning**: Ensure that the image names and the resolution are the same.

1. Navigate to the `react-pwa-reference-storefront` repository.
2. In the `app/src/ep.config.json` file, update the `cortexApi.scope` parameter with the store name.
3. In the `react-pwa-reference-storefront⁩/src⁩/images⁩/site-images` directory, add marketing content images.

    **Note**: You can reuse images and the layouts in various components if you don’t change the image name.

4. In the `react-pwa-reference-storefront⁩/src⁩/images⁩/carousel-images` directory, add carousel content images.
5. In the `react-pwa-reference-storefront⁩/src⁩/images⁩/manifest-images` directory, add icons and any default splash screen images.
6. In the `react-pwa-reference-storefront⁩/src⁩/images⁩/header-icons` directory, add header icon images for components.
7. In the `react-pwa-reference-storefront⁩/src⁩/images⁩/icons` directory, add icon images.
8. Upload all product, SKU, and other site images to a Content Management System (CMS).
    1. In the CMS, update all product and SKU image names to corresponding SKU codes.
    2. In the `react-pwa-reference-storefront⁩/app/src/ep.config.json` file, update the `skuImagesURL` parameter for each image to the corresponding path for the image in the CMS.
    3. Update the SKU or product file name to `%sku%` and site image names to `%fileName%`.
    The `%sku%` parameter is populated with the SKU code associated with each image and the  `%fileName%` parameter is populated with the associated file name. When you load the page, Cortex retrieves the image for the specific SKU code or file name. Cortex uses the file path that is stored in the `skuImagesURL` parameter to find the image in the CMS.

    **Note**: If Cortex fails to retrieve the `%fileName%` value from the CMS, the parameter is populated with the assets available locally in the `./app/src/images/site-images` directory.

9. Optional: In the `react-pwa-reference-storefront⁩/app/src/ep.config.json` file:
    1. Set the `arKit.enable` parameter to **true**.
    If a usdz file for a SKU is available at the path provided by the `skuImagesURL` parameter, the product images are wrapped with an anchor tag with a reference to the file on the CMS.
    2. Update the `arKit.skuArImagesUrl` parameter to the usdz file paths hosted on the CMS for ARKit Quick Look images.
    3. To complete the URL of the files, in the file path, update the `sku/file-name` parameter to `%sku%`. For example, `"skuArImagesUrl":"https://s3.amazonaws.com/referenceexp/ar/%sku%.usdz"`
    Cortex populates the page with the images corresponding to the parameter value when the page loads.
10. Update the `react-pwa-reference-storefront⁩/app/src/theme/common.less` file with the color values for the store and any other required properties.

    **Note**: You can specify custom paths for application and component images.

    * For application images, update the `imgURL` parameter to point to target directory.
    * For component images, update the `imgComponentUrl` parameter to point to a target directory.

    In either case, ensure that you retain any required sub-directory structures and naming conventions within the image directories.

11. Update the `react-pwa-reference-storefront⁩/components/src/style/common.less` file with the color values for the component.
