---
id: arkit
title: ARKit Quick Look
sidebar_label: ARKit Quick Look
---

## Apple ARKit Augmented Reality Quick Look Integration

The React PWA (Progressive Web App) Reference Storefront is pre-configured for integration with Apple’s ARKit web integration. For iOS 12 or later versions, you can incorporate 3D objects into the real world using AR Quick Look directly through the Safari web browser when you visit a product’s display page that supports the functionality. For more information about ARKit, see [Augmented Reality](https://developer.apple.com/augmented-reality/).

ARKit usdz files are externalized through content URLs within the storefront application configuration. The default URLs are configured to reference the usdz files, which are located on Amazon S3. However, you can use other CMS (Content Management System) providers as you prefer. When the image is called to the storefront, the required usdz files are retrieved on a per-SKU basis as they are available from the CMS provider. The storefront only displays the required AR tags, if the file exists. Any SKUs without a corresponding usdz file will not have an AR tag displayed on the product display page.

![Arkit](assets/AR-page-diagram.png)

Configuration properties for content URLs are:

* `arKit.enable`: Enable elements for ARKit’s Quick Look capability to load on a product display page. When `arKit.enable` is enabled, any product images that have hosted ARKit usdz files are wrapped with an anchor tag referencing the file hosted on an external CMS
* `arKit.skuArImagesUrl`: The path to the usdz files hosted on an external CMS used for ARKit Quick Look. Set this parameter to the complete URL of the files by replacing the `sku/file-name` parameter with `%sku%`. This parameter is populated when the page is loaded with values retrieved by Cortex

For any other CMS, you must update the configurations to reflect the public URLs of the files being retrieved by the particular content provider.
