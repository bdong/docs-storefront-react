---
id: ibm-wch
title: IBM Content Hub Integration
sidebar_label: IBM Content Hub
---

## Content Management for Assets and Catalog Images Integration Example

The React PWA (Progressive Web App) Reference Storefront is pre-configured for integration with various Content Management solutions by externalizing the content URLs through the storefront application configurations. The default URLs are configured to reference images, which are located on Amazon S3. However, you may use other CMS (Content Management System) providers as you prefer.

Configuration properties for content URLs are defined as follows:

* `skuImagesUrl`: Path to catalog images hosted on an external CMS. Set this to the complete URL of your images, replacing the sku/file-name with the string `%sku%`. This value is populated when the page is loaded with the values retrieved by Cortex
* `siteImagesUrl`: Path to site content and marketing images hosted on an external CMS. Set this to the complete URL of your images, replacing the file name and file extension with the string `%fileName%`. This value is populated when the page is loaded with the values retrieved by your components. If there is an issue with retrieving the values, the value is populated with assets locally available in `./app/src/images/site-images`

If you use another CMS, you must update the configurations to reflect the public URLs of the content being retrieved by the particular content provider.

### IBM Watson Content Hub (WCH) Integration Example

IBM Watson Content Hub is a cloud-based CMS. It also provides services with IBM Watson, such as cognitive tagging, to help transform assets into a searchable library of content.

You can upload catalog images and site content images to WCH using the wchtools command-line utility. Uploaded assets are scanned using the Watson’s Visual Recognition service, which are tagged based on the content, to create a searchable library. After uploading the images, provide the content delivery URL with the appropriate sku/file name placeholders in the Reference Storefront’s configuration for both `skuImagesUrl` and `siteImagesUrl`. In the following examples, a few fields are populated based on the tenant ID of the WCH account and the directory structure of the assets:

```json
{
  "skuImagesUrl": "https://my11.digitalexperience.ibm.com/<wch_tenant_identifier>/dxdam/<catalog_directory>/%sku%.jpeg",
  "siteImagesUrl": "https://my11.digitalexperience.ibm.com/<wch_tenant_identifier>/dxdam/<site_images>/%fileName%",
}
```

## Related Documentation

* [Watson Content Hub Login to Digital Experience Dashboard](https://idaas.iam.ibm.com/idaas/oidc/endpoint/default/authorize?response_type=code&client_id=YjFjOWE5YmMtMzZhMy00&redirect_uri=https%3A%2F%2Fapi.customer-engagement.ibm.com%2Fauth%2Fcallback&scope=openid%20openid)
* [IBM Watson Content Hub Developer Tools CLI Repository](https://github.com/ibm-wch/wchtools-cli)
